Using XMLTV (in Swedish)
==============

Text är från [forum.tvsajten.com](http://forum.tvsajten.com//content/view/18/48/) och skriven av Mattias Holmlund.

Den data om TV-program som finns på XMLTV.se är tänkt att användas i olika applikationer såsom PVR (Personal Video Recorder) och andra program som kan dra nytta av att veta vad det är på TV. Om du vill se tablåerna i din webläsare, så kan du göra det på [Honeybee.it](http://honeybee.it).

Idag finns det ett flertal program som kan hämta data från XMLTV.se. Det mest använda är tv_grab_se_tvzon och är skrivet i Perl. Det är en del av xmltv-projektet sedan version 0.5.37 och kan laddas ner från deras sida med filer.

tv_grab_se_tvzon fungerar bäst under Linux och *BSD, men det bör gå att få det att fungera under Windows också. Det finns en windows-version att ladda ner från xmltv-projektet.

Använda
-----------------

Det enklaste sättet att köra tv_grab_se_tvzon under Windows är med hjälp av xmltv.exe. Den kan du ladda ner från [Sourceforge](http://sourceforge.net/projects/xmltv/files/xmltv/0.5.67/). Ladda ner zip-filen och packa upp den någonstans på din hårddisk. Till exempel under c:\Program Files. Nu kommer du att ha en fil som heter xmltv.exe i en katalog som heter xmltv-0.X.XX.

Nu kan du konfigurera tv_grab_se_tvzon. Starta ett kommandofönster genom att klicka på Start-knappen, välja Kör... och skriva cmd.

Gå till katalogen där xmltv.exe ligger med cd.

Kör kommandot

```bash
  xmltv.exe tv_grab_se_tvzon --configure
```

Välj vilka kanaler du vill ha data för.

Nu kan du hämta data för dina kanaler till filen output.xml genom att köra

```bash
  xmltv.exe tv_grab_se_tvzon > output.xml
```

Nu behöver du bara få in data i ditt PVR-program eller få PVR-programmet att köra tv_grab_se_tvzon självt. Hur du gör detta bör stå i dokumentationen till ditt PVR-program.
