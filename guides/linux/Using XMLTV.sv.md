Using XMLTV (in Swedish)
==============

Text är från [forum.tvsajten.com](http://forum.tvsajten.com//content/view/18/48/) och skriven av Mattias Holmlund.

Den data om TV-program som finns på XMLTV.se är tänkt att användas i olika applikationer såsom PVR (Personal Video Recorder) och andra program som kan dra nytta av att veta vad det är på TV. Om du vill se tablåerna i din webläsare, så kan du göra det på [Honeybee.it](http://honeybee.it).

Idag finns det ett flertal program som kan hämta data från XMLTV.se. Det mest använda är tv_grab_se_tvzon och är skrivet i Perl. Det är en del av xmltv-projektet sedan version 0.5.37 och kan laddas ner från deras sida med filer.

tv_grab_se_tvzon fungerar bäst under Linux och *BSD, men det bör gå att få det att fungera under Windows också. Det finns en windows-version att ladda ner från xmltv-projektet.

Använda
-----------------

För att tv_grab_se_tvzon ska fungera måste du först installera senaste versionen av xmltv (apt-get, yum). Du måste också installera Perl-modulen HTTP::Cache::Transparent.

När du har installerat ovanstående så kan du starta tv_grab_se_swedb. Första gången du kör det måste du konfigurera en katalog där cachen ska sparas och vilka kanaler du vill ladda ner data för. Det gör du genom att köra:

```bash
  tv_grab_se_tvzon --configure
```

Sen kör du bara tv_grab_se_tvzon för att ladda ner data. Du kan även ange hur många dagar du vill ladda ner data för. Skriv kommandot nedan för att få vidare instruktioner.

```bash
  perldoc tv_grab_se_tvzon
```
