<!-- NOTE: That we do one issue per channel. If you want to request multiple please create one issue per channel. -->

/label ~"channel request"
/assign @jnylen

## Channel Name

<!-- Example: Bloomberg TV -->

## Available countries

<!-- Example: Europe, USA -->

## URL

<!-- Example: https://bloomberg.com -->

## Contact email / Press URL

<!-- If possible, please provide a email we can contact the channel at or a press url for us to register at. -->