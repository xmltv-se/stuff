Available channels for Sweden:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Sweden.xml.gz). Too long to list here.

Notes:
-----------------

 * Öppna Kanalen GBG is missing due to them not having a standard structure for their files.
