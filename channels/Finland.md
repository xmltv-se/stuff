Available channels for Finland:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Finland.xml.gz). Too long to list here.

Notes:
-----------------

 * Most finnish channels are updated manually. So expect some data to be out of date. The cost to make this automatic is above 10k EUR.
