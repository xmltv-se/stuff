Available channels for Poland:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Poland.xml.gz). Too long to list here.

Notes:
-----------------

 * Polsat channels missing due to hard to get in contact with.
 * nc+ channels missing due to their requirement of me having a company to represent to be allowed access to schedules.
