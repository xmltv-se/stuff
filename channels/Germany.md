Available channels for Germany:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Germany.xml.gz). Too long to list here.

Notes:
-----------------

 * RTL channels in Germany doesn't have Synopsis due to licensing issues and missing episode info due to we needing to fetch data elsewhere as they don't have files with no synopsis.
 * ProSieben channels doesn't have Synopsis due to licensing issues (VG Media).
 * SPORT1 is missing due to them not wanting to be added.
 * MGM and MGM HD is missing due to them not wanting to be added.
 * Several other channels are missing information aswell as they are VG Media licensed ([list here](https://www.vg-media.de/de/elektronische-programmf%C3%BChrer-epg/sendeunternehmen.html)).
