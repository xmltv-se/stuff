Available channels for Denmark:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Denmark.xml.gz). Too long to list here.

Notes:
-----------------

 * We are working on adding more local channels. It's just hard to contact them.
