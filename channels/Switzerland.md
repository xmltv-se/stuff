Available channels for Switzerland:
==============

Can be found [here](http://xmltv.xmltv.se/channels-Switzerland.xml.gz). Too long to list here.

Notes:
-----------------

 * Some German channels are missing data and completely missing for a reason. Please read the "Germany"-list for info.
